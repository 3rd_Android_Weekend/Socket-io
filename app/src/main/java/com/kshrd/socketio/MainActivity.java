package com.kshrd.socketio;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ooooo";
    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            socket = IO.socket("http://10.0.3.2:8080");
            socket.on(Socket.EVENT_CONNECT, onConnect());
            socket.on(Socket.EVENT_DISCONNECT, onDisconnect());
            socket.on("counter", onCounterEvent());
            socket.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    private Emitter.Listener onCounterEvent() {
        Emitter.Listener onCounterEventListener = new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = (JSONObject) args[0];
                        TextView tvCounter = (TextView) findViewById(R.id.tvCounter);
                        try {
                            tvCounter.setText(jsonObject.getString("number"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        return onCounterEventListener;
    }

    Emitter.Listener onConnect(){
        Emitter.Listener onConnectListener = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, "Connected");
            }
        };
        return onConnectListener;
    }

    Emitter.Listener onDisconnect(){
        Emitter.Listener onDisconnectListener = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, "DisConnected");
            }
        };
        return onDisconnectListener;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (socket.connected())
            socket.disconnect();
    }
}
